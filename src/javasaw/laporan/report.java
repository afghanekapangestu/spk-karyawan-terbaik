package javasaw.laporan;

import java.io.File;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javasaw.database.DatabaseMySQL;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class report {

    private Connection conn = new DatabaseMySQL().connectDB();
    private final String reportPath = ".\\src\\javasaw\\laporan\\";
    private final String logoFileName = "src/javasaw/laporan/logo.png"; // Nama file logo

    public void generateReport(String filename) {
        try {
            // Load the report file
            JasperReport report = (JasperReport) JRLoader.loadObjectFromFile(reportPath + filename + ".jasper");

            // Set parameters for the report
            Map<String, Object> parameters = new HashMap<>();
            String logoPath = getAbsolutePath(logoFileName);
            parameters.put("logoPath", logoPath); // Assuming "logoPath" is the parameter name in your report

            // Fill the report with data and parameters
            JasperPrint jprint = JasperFillManager.fillReport(report, parameters, conn);

            // Create and display the viewer for the generated report
            JasperViewer jviewer = new JasperViewer(jprint, false);
            jviewer.setDefaultCloseOperation(JasperViewer.DISPOSE_ON_CLOSE);
            jviewer.setVisible(true);
        } catch (JRException ex) {
            Logger.getLogger(report.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private String getAbsolutePath(String relativePath) {
        File file = new File(relativePath);
        return file.getAbsolutePath();
    }
}
