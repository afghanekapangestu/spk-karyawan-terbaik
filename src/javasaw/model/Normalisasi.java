/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package javasaw.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Afghan
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Normalisasi {
    int alternatif_id;
    int kriteria_id;
    double nilai_normalisasi;
}
