-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.30 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for javasaw
CREATE DATABASE IF NOT EXISTS `javasaw` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `javasaw`;

-- Dumping structure for table javasaw.alternatif
CREATE TABLE IF NOT EXISTS `alternatif` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama_alternatif` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table javasaw.alternatif: ~5 rows (approximately)
REPLACE INTO `alternatif` (`id`, `nama_alternatif`) VALUES
	(35, 'Rizky Samur'),
	(36, 'Retno Melani'),
	(37, 'Ayu Saraswati'),
	(38, 'Pahlevi'),
	(39, 'Vendi Sando');

-- Dumping structure for table javasaw.bobot
CREATE TABLE IF NOT EXISTS `bobot` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_kriteria` int DEFAULT NULL,
  `nilai_bobot` decimal(5,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_kriteria` (`id_kriteria`),
  CONSTRAINT `bobot_ibfk_1` FOREIGN KEY (`id_kriteria`) REFERENCES `kriteria` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table javasaw.bobot: ~0 rows (approximately)

-- Dumping structure for table javasaw.kriteria
CREATE TABLE IF NOT EXISTS `kriteria` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama_kriteria` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `tipe_kriteria` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `bobot_kriteria` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table javasaw.kriteria: ~5 rows (approximately)
REPLACE INTO `kriteria` (`id`, `nama_kriteria`, `tipe_kriteria`, `bobot_kriteria`) VALUES
	(30, 'Kedisiplinan', 'Benefit', 25),
	(31, 'Kepuasan', 'Benefit', 25),
	(32, 'Kemampuan', 'Benefit', 20),
	(33, 'Prestasi Kerja', 'Benefit', 15),
	(34, 'Kehadiran', 'Benefit', 15);

-- Dumping structure for table javasaw.normalisasi
CREATE TABLE IF NOT EXISTS `normalisasi` (
  `id` int NOT NULL AUTO_INCREMENT,
  `alternatif_id` int NOT NULL,
  `kriteria_id` int NOT NULL,
  `nilai_normalisasi` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `alternatif_id` (`alternatif_id`),
  KEY `kriteria_id` (`kriteria_id`),
  CONSTRAINT `normalisasi_ibfk_1` FOREIGN KEY (`alternatif_id`) REFERENCES `alternatif` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `normalisasi_ibfk_2` FOREIGN KEY (`kriteria_id`) REFERENCES `kriteria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=226 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table javasaw.normalisasi: ~25 rows (approximately)
REPLACE INTO `normalisasi` (`id`, `alternatif_id`, `kriteria_id`, `nilai_normalisasi`) VALUES
	(201, 35, 30, 0.5789473684210527),
	(202, 35, 31, 0.4444444444444444),
	(203, 35, 32, 0.5172413793103449),
	(204, 35, 33, 0.7653061224489796),
	(205, 35, 34, 1),
	(206, 36, 30, 1),
	(207, 36, 31, 0.8666666666666667),
	(208, 36, 32, 1),
	(209, 36, 33, 1),
	(210, 36, 34, 1),
	(211, 37, 30, 1),
	(212, 37, 31, 0.9888888888888889),
	(213, 37, 32, 0.8850574712643678),
	(214, 37, 33, 0.8979591836734694),
	(215, 37, 34, 1),
	(216, 38, 30, 1),
	(217, 38, 31, 1),
	(218, 38, 32, 0.7586206896551724),
	(219, 38, 33, 0.9183673469387755),
	(220, 38, 34, 1),
	(221, 39, 30, 1),
	(222, 39, 31, 0.7777777777777778),
	(223, 39, 32, 0.8850574712643678),
	(224, 39, 33, 0.9183673469387755),
	(225, 39, 34, 1);

-- Dumping structure for table javasaw.ranking
CREATE TABLE IF NOT EXISTS `ranking` (
  `id` int NOT NULL AUTO_INCREMENT,
  `alternatif_id` int NOT NULL,
  `skor_akhir` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `alternatif_id` (`alternatif_id`),
  CONSTRAINT `ranking_ibfk_1` FOREIGN KEY (`alternatif_id`) REFERENCES `alternatif` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table javasaw.ranking: ~0 rows (approximately)
REPLACE INTO `ranking` (`id`, `alternatif_id`, `skor_akhir`) VALUES
	(46, 35, 0.6241),
	(47, 36, 0.9667),
	(48, 37, 0.9589),
	(49, 38, 0.9395),
	(50, 39, 0.9092);

-- Dumping structure for table javasaw.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table javasaw.users: ~1 rows (approximately)
REPLACE INTO `users` (`id`, `username`, `password`, `created_at`, `updated_at`) VALUES
	(1, 'afghan', 'afghan', '2024-05-11 08:29:21', '2024-05-11 08:29:21');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
